FROM centos:7

MAINTAINER benjamin.shaw@manchester.ac.uk

WORKDIR /home

##############################################################################################
# Install 'OS' software.
##############################################################################################
RUN yum -y install epel-release  && \
    yum -y update && yum -y install \
    tcsh \
    unzip \
    wget \
    make \
    autoconf \
    automake \
    gcc \
    gcc-gfortran \
    libtool \
    gcc-c++ \
    cfitsio \
    cfitsio-devel \
    fftw-devel \
    which \
    git


##############################################################################################
## Install python modules
###############################################################################################

RUN yum -y install \
    python3 \
    python3-pip && \
    pip3 install --no-cache-dir --upgrade pip  && \
    pip3 install --no-cache-dir pip -U  && \
    pip3 install --no-cache-dir setuptools -U  && \
    pip3 install --no-cache-dir numpy -U && \
    pip3 install --no-cache-dir scipy -U  && \
    pip3 install --no-cache-dir matplotlib -U && \
    pip3 install --no-cache-dir pyephem -U && \
    pip3 install git+https://bitbucket.org/mkeith/filtools && \
    pip3 install --no-cache-dir PyYAML && \
    pip3 install --no-cache-dir lmfit && \ 
    pip3 install --no-cache-dir pytest && \ 
    pip3 install --no-cache-dir gitpython


RUN yum -y install tkinter

##############################################################################################
# Setup environment variables
##############################################################################################

# Make the directory where software will be installed. Note the -p flag tells mkdir to
# also create parent directories as required.
RUN mkdir -p /home/psr/soft

# Define home, psrhome, OSTYPE
ENV HOME=/home
ENV PSRHOME=/home/psr/soft
ENV OSTYPE=linux

# Python packages
ENV PYTHONPATH=/usr/local/lib/python3.6/site-packages/
RUN alias python='python3.6'

# psrcat
ENV PSRCAT_FILE=$PSRHOME/psrcat_tar/psrcat.db
ENV PATH=$PATH:$PSRHOME/psrcat_tar

# Tempo
ENV TEMPO=$PSRHOME/tempo
ENV PATH=$PATH:$PSRHOME/tempo/bin

# Tempo2
ENV TEMPO2=$PSRHOME/tempo2/T2runtime
ENV PATH=$PATH:$PSRHOME/tempo2/T2runtime/bin
ENV C_INCLUDE_PATH=$C_INCLUDE_PATH:$PSRHOME/tempo2/T2runtime/include
ENV LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PSRHOME/tempo2/T2runtime/lib

# SIGPROC
# These flags assist with the Sigproc compilation process, so do not remove them. If you take
# them out, then Sigproc will not build correctly.
ENV SIGPROC=$PSRHOME/sigproc
ENV PATH=$PATH:$SIGPROC/install/bin
ENV FC=gfortran
ENV F77=gfortran
ENV CC=gcc
ENV CXX=g++


##############################################################################################
# PULSAR SOFTWARE PIPELINE
##############################################################################################
WORKDIR /home

# Download the software from Rob
RUN wget https://github.com/scienceguyrob/SKA-TestVectorGenerationPipeline/raw/master/Deploy/Software/08_12_2016/Sigproc_MJK_SNAPSHOT_08_12_2016.zip && \
    wget https://github.com/scienceguyrob/SKA-TestVectorGenerationPipeline/raw/master/Deploy/Software/08_12_2016/Tempo_SNAPSHOT_08_12_2016.zip && \
    wget https://github.com/scienceguyrob/SKA-TestVectorGenerationPipeline/raw/master/Deploy/Software/08_12_2016/Tempo2_2016.11.3_SNAPSHOT_08_12_2016.zip && \
    unzip Sigproc_MJK_SNAPSHOT_08_12_2016.zip -d /home/Sigproc_MJK_SNAPSHOT_08_12_2016 && \
    unzip Tempo_SNAPSHOT_08_12_2016.zip -d /home/Tempo_SNAPSHOT_08_12_2016 && \
    unzip Tempo2_2016.11.3_SNAPSHOT_08_12_2016.zip -d /home/Tempo2_2016.11.3_SNAPSHOT_08_12_2016 && \
    rm Sigproc_MJK_SNAPSHOT_08_12_2016.zip && \  
    rm Tempo_SNAPSHOT_08_12_2016.zip && \
    rm Tempo2_2016.11.3_SNAPSHOT_08_12_2016.zip && \
    mv /home/Sigproc_MJK_SNAPSHOT_08_12_2016 /home/psr/soft/sigproc && \
    mv /home/Tempo_SNAPSHOT_08_12_2016 /home/psr/soft/tempo && \
    mv /home/Tempo2_2016.11.3_SNAPSHOT_08_12_2016 /home/psr/soft/tempo2 && \
    rm /home/psr/soft/sigproc/__MACOSX -R && \
    rm /home/psr/soft/tempo/__MACOSX -R && \
    rm /home/psr/soft/tempo2/__MACOSX -R

##############################################################################################
# TEMPO Installation
##############################################################################################
WORKDIR $PSRHOME/tempo
RUN ./prepare && \
    ./configure --prefix=$PSRHOME/tempo && \
    make && \
    make install && \
    mv obsys.dat obsys.dat_ORIGINAL && \
    wget https://raw.githubusercontent.com/mserylak/pulsar_docker/2f15b0d01b922d882b67ec32674d162f41b80377/tempo/obsys.dat

##############################################################################################
# TEMPO2 Installation
##############################################################################################

WORKDIR $PSRHOME/tempo2
RUN ./bootstrap && \
    ./configure --x-libraries=/usr/lib/x86_64-linux-gnu --enable-shared --enable-static --with-pic F77=gfortran && \
    make && \
    make install && \
    make plugins-install
WORKDIR $PSRHOME/tempo2/T2runtime/observatory
RUN mv observatories.dat observatories.dat_ORIGINAL && \
    mv oldcodes.dat oldcodes.dat_ORIGINAL && \
    mv aliases aliases_ORIGINAL && \
    wget https://raw.githubusercontent.com/mserylak/pulsar_docker/2f15b0d01b922d882b67ec32674d162f41b80377/tempo2/observatories.dat && \
    wget https://raw.githubusercontent.com/mserylak/pulsar_docker/2f15b0d01b922d882b67ec32674d162f41b80377/tempo2/aliases

##############################################################################################
# Sigproc Installation
##############################################################################################
WORKDIR $SIGPROC
RUN ./bootstrap && \
    ./configure --prefix=$SIGPROC/install LDFLAGS="-L/home/psr/soft/tempo2/T2runtime/lib" LIBS="-ltempo2" && \
    make && \
    make install

WORKDIR /home/psr/soft
RUN git clone https://gitlab.com/ska-telescope/pss-test-vector-generator.git vectortools
WORKDIR /home/psr/soft/vectortools
RUN chmod +x *.py

RUN  sed -i '1s/^/#!\/usr\/bin\/python3.6\n/' /home/psr/soft/vectortools/GenerateGaussian.py && \
     sed -i '1s/^/#!\/usr\/bin\/python3.6\n/' /home/psr/soft/vectortools/GenerateNoise.py && \
     sed -i '1s/^/#!\/usr\/bin\/python3.6\n/' /home/psr/soft/vectortools/fluxCalc.py && \
     sed -i 's/python/python3.6/g' /home/psr/soft/vectortools/Generator.py
ENV PATH="/home/psr/soft/vectortools/:${PATH}"

ENTRYPOINT /bin/bash
