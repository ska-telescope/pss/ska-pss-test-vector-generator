
"""
    **************************************************************************
    |                                                                        |
    |                    PSS Test Vector Noise Generator                     |
    |                                                                        |
    **************************************************************************
    | Description:                                                           |
    |                                                                        |
    | Noise filterbank generator. This class is a wrapper to fast_fake       |
    | and is used to produce filterbank files containing Gaussian Noise only |
    |                                                                        |
    **************************************************************************
    | Author: Benjamin Shaw                                                  |
    | Email : benjamin.shaw@manchester.ac.uk                                 |
    **************************************************************************
    | Optional command Line Arguments:                                       |
    |                                                                        |
    | --tobs (float)        Observation duration (s)                         |
    |                       (default=600)                                    |
    |                                                                        |
    | --tsamp (float)       Sampling interval (microseconds)                 |
    |                       (default=64)                                     |
    |                                                                        |
    | --mjd (float)         The timestamp of the first sample (MJD)          |
    |                       (default=56000.0)                                |
    |                                                                        |
    | --fch1 (float)        The frequency of the first channel (MHz)         |
    |                       (default=1670.0)                                 |
    |                                                                        |
    | --chbw (float)        The channel bandwidth (MHz)                      |
    |                       (default=-0.078125) (should be -ve by convention |
    |                                                                        |
    | --nbits (int)         The number of bits per sample                    |
    |                       (default=8)                                      |
    |                                                                        |
    | --nchans (int)        The number of frequency channels                 |
    |                       (default=4096)                                   |
    |                                                                        |
    | --name (string)       The name of the simulated source                 |
    |                       (default="test")                                 |
    |                                                                        |
    | --output (string)     The name of the output file                      |
    |                       (default="noise.fil")                            |
    |                                                                        |
    | --seed (int)          The random number seed for noise generation      |
    |                       (selected by fast_fake if not supplied)          |
    **************************************************************************
    | License:                                                               |
    |                                                                        |
    | Copyright 2021 University of Manchester                                |
    |                                                                        |
    |Redistribution and use in source and binary forms, with or without      |
    |modification, are permitted provided that the following conditions are  |
    |met:                                                                    |
    |                                                                        |
    |1. Redistributions of source code must retain the above copyright       |
    |notice,                                                                 |
    |this list of conditions and the following disclaimer.                   |
    |                                                                        |
    |2. Redistributions in binary form must reproduce the above copyright    |
    |notice, this list of conditions and the following disclaimer in the     |
    |documentation and/or other materials provided with the distribution.    |
    |                                                                        |
    |3. Neither the name of the copyright holder nor the names of its        |
    |contributors may be used to endorse or promote products derived from    |
    |this                                                                    |
    |software without specific prior written permission.                     |
    |                                                                        |
    |THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS     |
    |"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT       |
    |LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A |
    |PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT      |
    |HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  |
    |SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT        |
    |LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,   |
    |DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON       |
    |ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR      |
    |TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  |
    |USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH        |
    |DAMAGE.                                                                 |
    **************************************************************************
"""

from __future__ import print_function
import argparse
import logging
import subprocess
import os

logging.basicConfig(format='1|%(asctime)s|%(levelname)s|%(funcName)s|%(filename)s#%(lineno)d|%(message)s',
                    datefmt='%Y-%m-%dT%I:%M:%S',
                    level=logging.INFO)

class NoiseGenerate():

    def __init__(self, tobs, tsamp, mjd, fch1, chbw, nbits, nchans, name, output=False, seed=False):

        self.tobs = tobs       # Observation length (s)
        self.tsamp = tsamp      # Sampling time (us)
        self.mjd = mjd        # MJD of first sample
        self.fch1 = fch1       # Frequency of channel 1 (MHz)
        self.chbw = chbw       # Channel bandwidth (MHz)
        self.nbits = nbits      # Number of bits
        self.nchans = nchans     # Number of channels
        self.name = name       # Source name
        self.output = output     # Output filename
        self.seed = seed       # Random seed
        self.com = None
        if self.seed and not self.output:
            self.output = "noise_seed=" + str(self.seed) + ".fil"
        elif self.seed and self.output:
            self.output = self.output
        else:
            self.output = "noise_seed.fil"
        
    def form_command(self):
        '''
        Sets up the call to fast_fake
        '''
        if not self.seed:
            command = ['fast_fake',
                       '-T', str(self.tobs),
                       '-t', str(self.tsamp),
                       '-m', str(self.mjd),
                       '-F', str(self.fch1),
                       '-f', str(self.chbw),
                       '-b', str(self.nbits),
                       '-c', str(self.nchans),
                       '-s', str(self.name),
                       '-o', str(self.output)]
        else:
            command = ['fast_fake',
                       '-T', str(self.tobs),
                       '-t', str(self.tsamp),
                       '-m', str(self.mjd),
                       '-F', str(self.fch1),
                       '-f', str(self.chbw),
                       '-b', str(self.nbits),
                       '-c', str(self.nchans),
                       '-s', str(self.name),
                       '-S', str(self.seed),
                       '-o', str(self.output)]
        return command

    @staticmethod
    def fast_fake(com):
        """
        Start fast_fake child process
        """
        child = subprocess.Popen(com, stdout=subprocess.PIPE)
        child.communicate()
        child.wait()

    @staticmethod
    def fexists(this_file):
        if not os.path.isfile(this_file):
            raise FileNotFoundError("Noise file not produced")
        else:
            logging.info("Noise file {} produced".format(this_file))

    def make_noise(self):
        """
        Main method and entrypoint
        """

        # Set up fast_fake command
        self.com = self.form_command()
        running_now = ' '.join(map(str, self.com))
        logging.info("Calling fast_fake with: {}".format(running_now))

        # Call fast_fake 
        self.fast_fake(self.com)

        # Check noise file was generated
        self.fexists(self.output)
        logging.info("Noise generation completed")

          



def main():

    # Process command line args
    parser = argparse.ArgumentParser(description='Test vector noise file generator')
    parser.add_argument('-T','--tobs', help='Integration time (s)',
                        required=False, type=float, default=600.0)
    parser.add_argument('-t','--tsamp', help='Samplig time (us)',
                        required=False, type=float, default=64.0)
    parser.add_argument('-m','--mjd', help='MJD of first sample',
                        required=False, type=float, default=56000.0)
    parser.add_argument('-F','--fch1', help='Frequency of channel 1 (MHz)',
                        required=False, type=float, default=1670.0)
    parser.add_argument('-f','--chbw', help='Channel bandwidth (MHz)',
                        required=False, type=float, default=-0.078125)
    parser.add_argument('-b','--nbits', help='Number of bits',
                        required=False, type=int, default=8)
    parser.add_argument('-c','--nchans', help='Number of channels',
                        required=False, type=int, default=4096)
    parser.add_argument('-s','--name', help='Source name',
                        required=False, type=str, default='test')
    parser.add_argument('-o','--output', help='Output filename',
                        required=False, type=str)
    parser.add_argument('-S','--seed', help='Random seed',
                        required=False, type=int)

    args = parser.parse_args()

    generate = NoiseGenerate(args.tobs,
                             args.tsamp,
                             args.mjd,
                             args.fch1,
                             args.chbw,
                             args.nbits,
                             args.nchans,
                             args.name,
                             args.output,
                             args.seed)
    generate.make_noise()


if __name__ == '__main__':
    print(__doc__)
    main()
