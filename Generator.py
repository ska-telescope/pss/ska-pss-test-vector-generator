#!/usr/bin/env python

"""
    **************************************************************************
    |                                                                        |
    |                      PSS Test Vector Generator                         |
    |                                                                        |
    **************************************************************************
    | Description:                                                           |
    |                                                                        |
    | Automates the use of ft_inject_pulsar and fast_fake to create          |
    | filterbank files containing simulated pulsar and single pulse signals  |
    |                                                                        |
    **************************************************************************
    | Author: Benjamin Shaw                                                  |
    | Email : benjamin.shaw@manchester.ac.uk                                 |
    | Author: Lina Levin Preston                                             |
    | Email : lina.preston@manchester.ac.uk                                  |
    **************************************************************************
    | Single vector generation command Line Arguments:                       |
    |                                                                        |
    | --noise (string)      path to a pre-existing noise file in which to    |
    |                       inject a pulsar signal                           |
    |                                                                        |
    | --profile (string)    full path to the output directory where the      |
    |                       output files will be stored.                     |
    |                                                                        |
    | --dm (float)          the signal dispersion measure (pc/cc)            |
    |                       (default=100)                                    |
    |                                                                        |
    | --freq (float)        The spin frequency of the injected pulsar        |
    |                       signal (Hz) (default=1)                          |
    |                                                                        |
    | --accn (float)        The acceleration of the injected pulsar          |
    |                       signal (default=0)                               |
    |                                                                        |
    | --sn (float)          The fold signal-to-noise ratio of the            |
    |                       injected pulsar signal (default=100)             |
    |                                                                        |
    | --tsys (float)        The system temperature of the observing system   |
    |                       (K) (default=25)                                 |
    |                                                                        |
    | --gain (float)        The gain of the observing system (K/Jy)          |
    |                       (default=1)                                      |
    |                                                                        |
    | --npol (int)          The number of polarisations observed             |
    |                       (default=1)                                      |
    |                                                                        |
    | --location (string)    The path to a directory which will containing   |
    |                       the generated vector files. If this location     |
    |                       does not exist it will be created.               |
    |                                                                        |
    | --boxcar              Use this flag if the injected signal is a        |
    |                       boxcar function (def=False)                      |
    |                                                                        |
    | --smear               Use this flag to allow intra-channel DM          |
    |                       in the injected signal (def=False)               |
    |                                                                        |
    | --nogen               Just print command instead of forming vectors    |
    |                       (for debuggin) (def=False)                       |
    |                                                                        |
    | --vtype (string)      The specific vector type.                        |
    |                       DDTR, SPS, FDAS-ACC, FDAS-PER, FDAS-FOP,         |
    |                       FDAS-SLOW, FLDO for production mode. Any type    |
    |                       can be set in non-production mode.               |
    |                                                                        |
    | --prod                Flag to set production mode as ON. In this mode  |
    |                       vectors will be written to the SKA test vector   |
    |                       repository on dokimi.ast.man.ac.uk. This mode    |
    |                       will not work on any other machine. This mode    |
    |                       will be overridden if --location is used.        |
    |                                                                        |
    **************************************************************************
    | Batch generation mode command Line Arguments:                          |
    |                                                                        |
    |  --batch (string)      Batch mode. Make batches of one or more vectors |
    |                        Takes yaml file containing sets of              |
    |                        test vector parameters. The yaml file should    |
    |                        contain all noise, telescope, profile and       |
    |                        vector parameters (including vtype). See        |
    |                        examples directory for more info                |
    |                                                                        |
    | --location (string)    The path to a directory which will containing   |
    |                        the generated vector files. If this location    |
    |                        does not exist it will be created.              |
    |                                                                        |
    | --prod                 Flag to set production mode as ON. In this mode |
    |                        vectors will be written to the SKA test vector  |
    |                        repository on dokimi.ast.man.ac.uk. This mode   |
    |                        will not work on any other machine. This mode   |
    |                        will be overridden if --location is used.       |
    |                                                                        |
    | --seed (int)           Random number seed to use for noise file        |
    |                        generation. If not provided, system UNIX        |
    |                        time stamp will be used.                        |
    |                                                                        |
    | --noclean              Do not remove profiles and noise files after    |
    |                        generation completes.                           |
    |                                                                        |
    | --nocheck              If not selected, Batch mode will prompt user    |
    |                        with information about the number of size of the|
    |                        dataset that is about to be produced and        |
    |                        requires user confirmation to proceed. In       |
    |                        production mode (batch or single vector),       |
                             a warning and prompt are also                   |
    |                        produced before vectors are generated.          |
    |                        --nocheck should be used when running this code |
    |                        as part of scripts.                             |
    | --noise (string)      path to a pre-existing noise file in which to    |
    |                       inject a pulsar signal                           |
    **************************************************************************
    | License:                                                               |
    |                                                                        |
    | Copyright 2021 University of Manchester                                |
    |                                                                        |
    |Redistribution and use in source and binary forms, with or without      |
    |modification, are permitted provided that the following conditions are  |
    |met:                                                                    |
    |                                                                        |
    |1. Redistributions of source code must retain the above copyright       |
    |notice,                                                                 |
    |this list of conditions and the following disclaimer.                   |
    |                                                                        |
    |2. Redistributions in binary form must reproduce the above copyright    |
    |notice, this list of conditions and the following disclaimer in the     |
    |documentation and/or other materials provided with the distribution.    |
    |                                                                        |
    |3. Neither the name of the copyright holder nor the names of its        |
    |contributors may be used to endorse or promote products derived from    |
    |this                                                                    |
    |software without specific prior written permission.                     |
    |                                                                        |
    |THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS     |
    |"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT       |
    |LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A |
    |PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT      |
    |HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  |
    |SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT        |
    |LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,   |
    |DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON       |
    |ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR      |
    |TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  |
    |USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH        |
    |DAMAGE.                                                                 |
    **************************************************************************
"""

import argparse
import logging
import os
import shutil
import subprocess
import time
import git
import yaml
from fluxCalc import GetFlux
from GenerateGaussian import MakeGaussianProfile
from GenerateNoise import NoiseGenerate
from vheader import VHeader

# pylint: disable=W1202
# pylint: disable=C0301
# pylint: disable=E1101
# pylint: disable=R0914

logging.basicConfig(format='1|%(asctime)s|%(levelname)s|%(funcName)s|%(module)s#%(lineno)d|%(message)s',
                    datefmt='%Y-%m-%dT%I:%M:%S',
                    level=logging.INFO)

class Batch():

    def __init__(self, yamlfile,
                 seed, outpath=None,
                 prod=False, noclean=False,
                 nocheck=False, rfi_smooth=0,
                 noise = None):

        self.yamlfile = yamlfile
        self.seed = seed
        self.outpath = outpath
        self.prod = prod
        self.noclean = noclean
        self.nocheck = nocheck
        self.rfi_smooth = rfi_smooth
        self.noise = noise
        if self.outpath:
            self.prod = False

        if self.outpath == None and self.prod == False:
            raise Exception("No output file location specified")

    @staticmethod
    def fexists(this_file):
        """
        Checks a file exists. True if yes, else False.
        """
        if not os.path.isfile(this_file):
            return False
        return True

    @staticmethod
    def load_yaml(yamlfile):
        """
        Loads in the YAML file. Checks it's valid YAML.
        Exception raised if not.
        """
        with open(yamlfile, 'r') as this_yaml:
            try:
                pars = yaml.load(this_yaml, Loader=yaml.FullLoader)
            except yaml.parser.ParserError:
                logging.error("{} not valid YAML".format(yamlfile))
                raise Exception("YamlError")
        return pars

    @staticmethod
    def check_rfi(data, noise):
        """
        Checks if configurations provided in the RFI YAML is 
        valid for the given noise Parameters or not.
        """
        fch2 = noise['fch1'] + noise['nchans']*noise['chbw']
        for i in data:
            if fch2>i['fstart'] or noise['fch1']<i['fstop'] or noise['tobs']<i['tstop'] or i['tstart']<0:
                return False
        return True

    @staticmethod
    def check_load(data):
        """
        Calculates the total number of vectors that will
        be produced and estimates the disk size required.
        """
        noise_pars = data["noise"]
        tobs = noise_pars["tobs"]
        tsamp = noise_pars["tsamp"] * 1e-06
        nchans = noise_pars["nchans"]
        file_size = (tobs / (tsamp)) *  nchans

        profile_pars = data["profiles"]
        nprofs = len(profile_pars["widths"])

        nfiles = 0
        vectors_list = data["vectors"]
        for subset in vectors_list:
            this_subset_size = 0
            this_dict = vectors_list[subset]

            dmlist = len(this_dict["dms"])
            this_subset_size += dmlist

            accns = len(this_dict["accelerations"])
            this_subset_size *= accns

            frequencies = len(this_dict["frequencies"])
            this_subset_size *= frequencies

            snrs = len(this_dict["snrs"])
            this_subset_size *= snrs

            this_subset_size *= nprofs

            nfiles += this_subset_size

        try:
            if data['rfi']:
                nfiles = nfiles*len(data['rfi'])
        except KeyError:
            pass
        disk = (nfiles * file_size) / 1e9
        logging.warning("You are generating {} vectors totalling {} GB of disk space".format(nfiles, disk))
        response = input("Are you sure you want to continue? (Yes/yes): ")
        if response.lower() != "yes":
            raise Exception("Aborted by user")

    @staticmethod
    def cleanup(profloc, noise_filename):
        logging.info("Deleting {}".format(noise_filename))
        os.remove(noise_filename)
        logging.info("Deleting {}".format(profloc))
        shutil.rmtree(profloc)

    def generate_batch(self):

        """
        Main batch mode method.
        """

        # Does our yaml file exist?
        if not self.fexists(self.yamlfile):
            raise FileNotFoundError("{} not found".format(self.yamlfile))

        # Load all yaml parameters
        pars = self.load_yaml(self.yamlfile)

        # Does the RFI file exist that matches with the ID?
        # We have the rfi_parent_dir hardcoded here
        rfi_parent_dir, rfi_list = './rfi_configs/',[]
        try:
            rfi_dict = pars['rfi']
        except KeyError:
            rfi_dict = []
        if rfi_dict:
            for i in rfi_dict:
                file_found = False
                for j in os.listdir(rfi_parent_dir):
                    if i in j and len(i)==4:
                        file_found = True
                        rfi_list.append(os.path.join(rfi_parent_dir,j))
                        logging.info('RFI File ID:{} found'.format(i))
                if not file_found:
                    raise FileNotFoundError("RFI file associated with ID:{} not found".format(i))
        logging.info('{} RFI configuration files found'.format(len(rfi_list)))

        # Check the user is happy with the data size
        if not self.nocheck:
            self.check_load(pars)

        # Check the user knows they are running in production mode.
        if not self.nocheck and not self.outpath:
            warn_str = "You are running a batch generation in production mode. Are you sure want to continue? (Yes/yes): "
            response = input(warn_str)
            if response.lower() != "yes":
                raise Exception("Aborted by user")
            else:
                self.nocheck = True

        # Check if there is a user-defined noise file, if not generate one
        noise_filename = ""
        if self.noise is not None:
            logging.info("Using pre-existing noise file")

            if not self.fexists(self.noise):
                raise FileNotFoundError("Noise file {} does not exist".format(self.noise))
            noise_filename = self.noise
            noise_dict = pars["noise"]

        else:
            # Do we have a seed?
            if not self.seed:
                logging.info("Seed not set. Using UNIX time stamp")
                self.seed = int(time.time())
            else:
                logging.info("Using seed {}".format(self.seed))

            # Make the noise file
            ## Get the noise parameters from yaml
            noise_dict = pars["noise"]

            ## Set name of noise file
            noise_filename = "noise_seed=" + str(self.seed) + ".fil"
            logging.info("Calling noise generator: {}".format(noise_filename))

            ## Run noise generation
            NoiseGenerate(noise_dict["tobs"],
                          noise_dict["tsamp"],
                          noise_dict["mjd"],
                        noise_dict["fch1"],
                        noise_dict["chbw"],
                        noise_dict["nbits"],
                        noise_dict["nchans"],
                        noise_dict["name"],
                        noise_filename,
                        self.seed).make_noise()
        
        # Checking the RFI configuration specified, fits into the filterbank noise configuration
        rfi_dict = []
        for i in rfi_list:
            rfi_dict.append(self.load_yaml(i))
        for rfi in range(len(rfi_list)):
            if self.check_rfi(rfi_dict[rfi],noise_dict):
                logging.info("RFI Configuration file:{} - check passed.".format(rfi_list[rfi]))
            else:
                raise Exception("RFI File with ID {} cannot be processed".format(rfi_list[rfi]))


        # Make profiles
        ## Get profile widths and resolution
        prof_dict = pars["profiles"]
        nbins = prof_dict["nbins"]
        widths = prof_dict["widths"]

        ## Set location for profiles
        profloc = "profiles"
        if os.path.isdir(profloc):
            logging.info("Profile directory already exists: Appending new profiles")
        else:
            os.mkdir(profloc)
            logging.info("Profile directory '{}' created".format(profloc))

        ## Make profiles
        for width in widths:
            MakeGaussianProfile(nbins, width, profloc,
                                show=False, norm=True).generate_profile()

        ## Get profiles as array:
        profiles_list = os.listdir(profloc)

        # If there is no RFI specified, appending 'None' into the list to continue the injection of pulsar
        if not rfi_list:
            rfi_list.append(None)
        # Get telescope parameters
        telescope = pars["telescope"]
        gain = telescope["gain"]
        npol = telescope["npol"]
        tsys = telescope["tsys"]

        # Make vectors:
        ## Get vectors dictionary from yaml
        vectors_list = pars["vectors"]
        for subset in vectors_list:
            this_dict = vectors_list[subset]
            dmlist = this_dict["dms"]
            freqlist = this_dict["frequencies"]
            acclist = this_dict["accelerations"]
            snrlist = this_dict["snrs"]
            this_vtype = this_dict["vtype"]

            ## Do generations
            for this_prof in profiles_list:
                prof_path = profloc + "/" + this_prof
                for this_dm in dmlist:
                    for this_freq in freqlist:
                        for this_acc in acclist:
                            for this_snr in snrlist:
                                for this_rfi in rfi_list:
                                    if self.prod:
                                        Generate(noise_filename,
                                                 prof_path,
                                                 this_dm,
                                                 this_freq,
                                                 this_acc,
                                                 this_snr,
                                                 tsys,
                                                 gain,
                                                 npol,
                                                 vtype=this_vtype,
                                                 prod=self.prod,
                                                 nocheck=self.nocheck,
                                                 rfi_file=this_rfi,
                                                 rfi_smooth=self.rfi_smooth).inject()
                                    else:
                                        Generate(noise_filename,
                                                 prof_path,
                                                 this_dm,
                                                 this_freq,
                                                 this_acc,
                                                 this_snr,
                                                 tsys,
                                                 gain,
                                                 npol,
                                                 outpath=self.outpath,
                                                 vtype=this_vtype,
                                                 nocheck=self.nocheck,
                                                 rfi_file=this_rfi,
                                                 rfi_smooth=self.rfi_smooth).inject()

                            logging.info("Injection complete\n\n")

        if not self.noclean and self.noise is None:
            self.cleanup(profloc, noise_filename)

class Generate():

    # Repo top level directory on dokimi.ast.man.ac.uk
    # If running in production mode on another machine
    # this path must be updated to reflect the location
    # of the vector repo
    REPO_TOP_LEVEL = "/skatvnas3/doc_root/testvectors"
    # List of valid vector types.
    VTYPES = ['TDAS-MID',
              'SPS-MID',
              'SPS-MID-RFI',
              'DDTR-MID',
              'FDAS-ACC-MID',
              'FDAS-PER-MID',
              'FDAS-FOP-MID',
              'FDAS-SLOW-MID',
              'FDAS-HSUM-MID',
              'FLDO-MID',
              'TEST']

    def __init__(self, noise, profile, dm, freq,
                 accn, sn, tsys, gain, npol,
                 outpath=None, vtype="Default",
                 boxcar=False, nogen=False,
                 smear=False, prod=False, nocheck=False,
                 rfi_file=None, rfi_smooth=0):

        self.noise = noise
        self.outpath = outpath
        self.outfile = None
        self.profile = profile
        self.dm = dm
        self.freq = freq
        self.accn = accn
        self.sn = sn
        self.tsys = tsys
        self.gain = gain
        self.npol = npol
        self.freq = freq
        self.duty = None
        self.boxcar = boxcar
        self.vtype = vtype
        self.smear = smear
        self.nogen = nogen
        self.prod = prod
        self.nocheck = nocheck
        self.com = None
        self.rfi_file = rfi_file
        self.rfi_smooth = rfi_smooth

        if self.outpath is None and self.prod is False:
            raise Exception("No output file location specified")

    @staticmethod
    def fexists(this_file):
        """
        Checks if a file exists. True if yes, else False
        """
        if os.path.isfile(this_file):
            logging.info("File {} found".format(this_file))
            return True
        else:
            logging.info("File {} not found".format(this_file))
        return False

    @staticmethod
    def get_dc_from_prof(prof):
        """
        Uses the profile filename to extract
        the duty cycle
        """
        basename = os.path.basename(prof)
        segments = basename.split('_')
        field = segments[1]
        duty = float(field.split('=')[1])
        logging.info("Duty cycle is {}".format(duty))
        return duty

    def get_seed(self):
        """
        Extracts the random number seed value from the
        noise filename. If no seed is found a 'XXXX' is
        used instead.
        """
        try:
            seedval = os.path.splitext(self.noise)[0].split("seed=")[1]
        except IndexError:
            seedval = "XXXX"
        return seedval

    def name_outfile(self):
        """
        Constructs the name of the filterbank file to be
        produced
        """
        try:
            repo = git.Repo(search_parent_directories=False)
            sha = repo.git.rev_parse(repo.head.commit.hexsha, short=7)
        except git.exc.InvalidGitRepositoryError:
            sha = 1
        this_seed = self.get_seed()

        if self.boxcar:
            if self.rfi_file:
                outfile = str(self.vtype) + "_" + str(sha) + "_" + str(self.freq) + "_" + str(self.duty) + "_" + str(self.dm) + "_" + str(self.accn) + "_Square_" + str(self.sn) + "_" +str(self.extract(self.rfi_file)[1])+"_"+ str(this_seed) +".fil"
            else:
                outfile = str(self.vtype) + "_" + str(sha) + "_" + str(self.freq) + "_" + str(self.duty) + "_" + str(self.dm) + "_" + str(self.accn) + "_Square_" + str(self.sn) + "_" + '0000'+ '_' + str(this_seed) +".fil"
        else:
            if self.rfi_file:
                outfile = str(self.vtype) + "_" + str(sha) + "_" + str(self.freq) + "_" + str(self.duty) + "_" + str(self.dm) + "_" + str(self.accn) + "_Gaussian_" + str(self.sn) + "_" +str(self.extract(self.rfi_file)[1])+"_"+ str(this_seed) +".fil"
            else:
                outfile = str(self.vtype) + "_" + str(sha) + "_" + str(self.freq) + "_" + str(self.duty) + "_" + str(self.dm) + "_" + str(self.accn) + "_Gaussian_" + str(self.sn) + "_" + '0000' + '_' + str(this_seed) + ".fil"

        return outfile

    def form_command(self, flux):
        """
        Sets up the call to ft_inject_pulsar
        """
        outpath = self.outpath + "/" + self.outfile
        logging.info("Output path is {}".format(outpath))
        if not self.smear:
            command = ['ft_inject_pulsar',
                       self.noise,
                       '-p', self.profile,
                       '-S', str(flux),
                       '--f0', str(self.freq),
                       '-D', str(self.dm),
                       '--accn', str(self.accn),
                       '--pepoch', '56000.0',
                       '--tsys', str(self.tsys),
                       '--gain', str(self.gain),
                       '--npol', str(self.npol),
                       '-x',
                       '--remove-signed',
                       '--rfi-smooth', str(self.rfi_smooth),
                       '-o', outpath]
        else:
            command = ['ft_inject_pulsar',
                       self.noise,
                       '-p', self.profile,
                       '-S', str(flux),
                       '--f0', str(self.freq),
                       '-D', str(self.dm),
                       '--accn', str(self.accn),
                       '--pepoch', '56000.0',
                       '--tsys', str(self.tsys),
                       '--gain', str(self.gain),
                       '--npol', str(self.npol),
                       '--remove-signed',
                       '--rfi-smooth', str(self.rfi_smooth),
                       '-o', outpath]
        if self.rfi_file is not None:
            command.append('--rfi-config')
            command.append(self.rfi_file)
        this_command = ' '.join(map(str, command))
        logging.info("Injector command is: {}".format(this_command))
        return command

    @staticmethod
    def run_injection(command):
        """
        Runs a process as a subprocess. The ft_inject_pulsar
        command is passed into here and then ft_inject_pulsar is
        run as a child process.
        """
        child = subprocess.Popen(command, stdout=subprocess.PIPE)
        child.communicate()
        child.wait()

    def set_prod_path(self):
        """
        Production mode only. Checks the production repo
        exists and that the vector type (Vtype) is a valid
        type.
        """
        if not os.path.isdir(Generate.REPO_TOP_LEVEL):
            raise FileNotFoundError("Production mode repo not found")
        logging.info("Vector repo located at {}".format(Generate.REPO_TOP_LEVEL))
        if self.vtype not in Generate.VTYPES:
            logging.error("Vector type {} not valid - Exiting".format(self.vtype))
        logging.info("Vector type {} is valid:".format(self.vtype))
        outpath = str(Generate.REPO_TOP_LEVEL) + "/" + str(self.vtype)
        if not os.path.isdir(outpath):
            raise FileNotFoundError("Vector type subdirectory {} not found".format(outpath))
        return outpath

    @staticmethod
    def extract(vector):
        """
        Takes a test vector filename and extracts
        the random number seed and the gitlab hash,
        returning the remaining parameters as a list.
        """
        fields = vector.split("_")[:-1]
        fields.pop(1)
        return fields

    def check_latest(self, latest_path):
        """
        Compares the new vector with the vectors
        already in the "latest" direction in order
        to overwrite existing latest vectors if a
        new vector is produced with the same signal.

        This method will only run in production mode.
        """
        new_vector_fields = self.extract(self.outfile)
        current_vectors = os.listdir(latest_path)
        for vector in current_vectors:
            vector_fields = self.extract(vector)
            if new_vector_fields == vector_fields:
                logging.warning("Vector already exists: Updating latest version")
                os.remove(latest_path + "/" + vector)
                return True
        return False

    def inject(self):

        """
        Method to run vector generation
        """

        # Does an output directory for the vectors exist?
        if self.outpath:
            self.prod = False
            if os.path.isdir(self.outpath):
                logging.info("Output directory {} found".format(self.outpath))
            else:
                logging.info("Directory {} not found. Creating...".format(self.outpath))
                os.mkdir(self.outpath)
                logging.info("Directory {} created".format(self.outpath))

        # Are we in production mode?
        if self.prod:
            warn_string = "RUNNING IN PRODUCTION MODE - WARNING! This mode will add vectors to SKA test vector repository"
            logging.warning(warn_string)
            if not self.nocheck:
                response = input("Are you sure you want to continue? (yes/no): ")
                if response.lower() != "yes":
                    raise Exception("Aborted by user")
            self.outpath = self.set_prod_path()

        # Do we have a profile file?
        if not self.fexists(self.profile):
            raise FileNotFoundError("Profile not found")

        # Get parameters from the noise file and some telescope parameters
        noise_header = VHeader(self.noise)
        bandwidth = noise_header.bandwidth() * 1e6
        logging.info("Bandwidth is: {}".format(bandwidth))
        dur = noise_header.duration()
        logging.info("Obs duration is: {}".format(dur))
        logging.info("Telescope gain: {} K/Jy".format(self.gain))
        logging.info("System temperature: {} K".format(self.tsys))
        logging.info("Number of pols: {}".format(self.npol))

        # What is our duty cycle?
        self.duty = self.get_dc_from_prof(self.profile)

        # What is our flux?
        flux = GetFlux(self.sn,
                       bandwidth,
                       self.tsys,
                       self.gain,
                       dur,
                       self.npol,
                       self.duty, 1.0 / self.freq).flux()
        logging.info("Flux is {} Jy".format(flux))

        # What shall our test vector filename be?
        self.outfile = self.name_outfile()
        logging.info("Vector name will be {}".format(self.outfile))

        # Form the ft_inject_pulsar command
        self.com = self.form_command(flux)

        if not self.nogen:
            logging.info("Injecting signal into {}".format(self.noise))
            self.run_injection(self.com)
            logging.info("Injection complete")

        # Do softlinking to latest in prod
        if self.prod:
            latest_path = self.outpath + "/latest"
            if not os.path.isdir(latest_path):
                raise FileNotFoundError("Cannot apply softlinks. {} not found".format(latest_path))
            logging.info("Updating vector softlinks")
            if not self.check_latest(latest_path):
                logging.info("This is a new vector. Creating latest version")
            symlink_path = latest_path + "/" + self.outfile
            file_path = self.outpath + "/" + self.outfile
            os.symlink(file_path, symlink_path)

def main():

    """
    Main method and entrypoint
    """

    print(__doc__)

    # Process command line args
    parser = argparse.ArgumentParser(description='Test vector generator')
    parser.add_argument('-n', '--noise', help='Path to noise file',
                        required=False, type=str, default=None)
    parser.add_argument('-p', '--profile', help='Profile ascii file',
                        required=False, type=str)
    parser.add_argument('-d', '--dm', help='Dispersion measure',
                        required=False, type=float, default=100)
    parser.add_argument('-f', '--freq', help='Spin frequency',
                        required=False, type=float, default=1)
    parser.add_argument('-a', '--accn', help='Acceleration',
                        required=False, type=float, default=0)
    parser.add_argument('-s', '--sn', help='Required signal-to-noise',
                        required=False, type=float, default=100)
    parser.add_argument('-T', '--tsys', help='System temperature (K)',
                        required=False, type=float, default=25.0)
    parser.add_argument('-G', '--gain', help='Telescope Gain (K/Jy)',
                        required=False, type=float, default=1.0)
    parser.add_argument('-N', '--npol', help='Number of polarisations',
                        required=False, type=int, default=1)
    parser.add_argument('-l', '--location', help='Path to directory to store filterbank',
                        required=False, type=str)
    parser.add_argument('-b', '--boxcar', help='Input profiles are boxcars',
                        required=False, action='store_true')
    parser.add_argument('-i', '--smear', help='Allow intra-channel smearing',
                        required=False, action='store_true')
    parser.add_argument('-g', '--nogen', help='Just show commands - do not generate (debug)',
                        required=False, action='store_true')
    parser.add_argument('-t', '--vtype', help='Vector type (e.g., TDAS, FDAS)',
                        required=False, type=str, default="Default")
    parser.add_argument('-P', '--prod', help='Production mode',
                        required=False, action='store_true')
    parser.add_argument('-B', '--batch', help='Run in batch mode (inputs in yaml file)',
                        required=False, type=str)
    parser.add_argument('-S', '--seed', help='Random number seed for noise generation',
                        required=False, type=int)
    parser.add_argument('-c', '--noclean', help='Do not delete Batch mode intermediary files (noise, profiles)',
                        required=False, action='store_true')
    parser.add_argument('-C', '--nocheck', help='Disable warnings for disk usage and production mode',
                        required=False, action='store_true')
    parser.add_argument('-R','--rficonfig', help='RFI Configuration YAML File',
                        required=False)
    parser.add_argument('--smooth', help='Option to smoothen the inject RFI (--smooth 10 would smoothen RFI over 10 frequency channels and time samples)',
                        required=False, type=int, default=0)

    args = parser.parse_args()

    if args.batch:
        logging.info("Running in Batch mode. Reading parameters from {}".format(args.batch))
        batcher = Batch(args.batch, args.seed,
                        args.location, args.prod,
                        args.noclean, args.nocheck,
                        rfi_smooth=args.smooth,
                        noise = args.noise)
        batcher.generate_batch()
    else:
        logging.info("Reading parameters from command line")
        if args.profile:
            generator = Generate(args.noise, args.profile, args.dm,
                                 args.freq, args.accn, args.sn,
                                 args.tsys, args.gain, args.npol,
                                 args.location, args.vtype,
                                 args.boxcar, args.nogen,
                                 args.smear, args.prod,
                                 args.nocheck, rfi_file=args.rficonfig,
                                 rfi_smooth = args.smooth)
            generator.inject()
        else:
            logging.error("No profile supplied")


if __name__ == '__main__':
    main()
